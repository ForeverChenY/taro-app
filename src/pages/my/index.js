import { useState, useEffect }from 'react'
import { View } from '@tarojs/components';
import 	Taro  from '@tarojs/taro';
import { AtAvatar, AtList, AtListItem } from 'taro-ui'
import "taro-ui/dist/style/components/avatar.scss";
import "taro-ui/dist/style/components/list.scss";
import "taro-ui/dist/style/components/icon.scss";
import './index.less'

export default function MyComponent() {
	const [userInfo, setUserInfo] = useState({});

	
	useEffect(() => {
		Taro.getStorage({
			key: 'userInfo',
			success: (res = {}) => {
				console.log(res);
				setUserInfo(JSON.parse(res.data))
			}
		})
	}, [])
	return (
		<View className='my'>
			<View className='header'>
				<AtAvatar className='logo' size='large' circle image={userInfo.avatarUrl}></AtAvatar>
				<View className='name'>{userInfo.nickName}</View>
			</View>
			<View className='main'>
			<AtList>
				<AtListItem title='用户信息' arrow='right' />
				<AtListItem title='设置' arrow='right' onClick={() => Taro.openSetting({})} />
			</AtList>
			</View>
		</View>
	)
}
