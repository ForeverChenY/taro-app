import utilsPages from './utils/utils_taro_dev';

export default {
	/**页面路径列表 */
	pages: utilsPages(),

	darkmode: true,
	/**是否开启 debug 模式，默认false */
	debug: false,

	/**全局的默认窗口表现 */
	window: {
		navigationBarBackgroundColor: '#ffffff', /**导航栏背景颜色，如 #000000 */
		navigationBarTextStyle: 'white', /**导航栏标题颜色，仅支持 black / white */
		navigationBarTitleText: 'WeChat',/**导航栏标题文字内容 */
		navigationStyle: 'default ', /**RN、H5不支持 导航栏样式，仅支持以下值：default 默认样式；custom 自定义导航栏，只保留右上角胶囊按钮 */
		backgroundColor: '#ffffff', /**H5不支持  窗口的背景色 */
		backgroundTextStyle: 'dark', /**RN、H5不支持 下拉 loading 的样式，仅支持 dark / light */
		backgroundColorTop: '#ffffff', /**RN、H5不支持 顶部窗口的背景色，仅 iOS 支持 */
		backgroundColorBottom: '#ffffff', /**RN、H5不支持 底部窗口的背景色，仅 iOS 支持 */
		backgroundColorBottom: '#ffffff', /**RN、H5不支持 底部窗口的背景色，仅 iOS 支持 */
		enablePullDownRefresh: false, /**RN、H5不支持 是否开启当前页面的下拉刷新 */
		onReachBottomDistance: 50, /**RN、H5不支持 页面上拉触底事件触发时距页面底部距离，单位为 px */
		pageOrientation: 'portrait', /**RN、H5不支持 屏幕旋转设置，支持 auto / portrait / landscape 详见 响应显示区域变化 */
	},

	/**小程序接口权限相关设置 */
	permission: {
		'scope.userLocation': {
			desc: '你的位置信息将用于小程序位置接口的效果展示'
		}
	},
	/**底部 tab 栏的表现 */
	"tabBar": {
		color: '#ffffff', /**tab 上的文字默认颜色，仅支持十六进制颜色 */
		selectedColor: '#ffffff', /**tab 上的文字选中时的颜色，仅支持十六进制颜色 */
		backgroundColor: '#ffffff',/**tab 的背景色，仅支持十六进制颜色 */
		borderStyle: 'black',/**tabbar 上边框的颜色， 仅支持 black / white */
		list: [ /**其中 list 接受一个数组，只能配置最少 2 个、最多 5 个 tab。tab 按数组的顺序排序，每个项都是一个对象 */
			// {
			// 	pagePath: 'pages/index/index', /**页面路径，必须在 pages 中先定义 */
			// 	text: '首页', /**tab 上按钮文字 */
			// 	iconPath: 'static/images/home.jpeg', /**图片路径，icon 大小限制为40kb，建议尺寸为 81px * 81px，不支持网络图片。当 position 为 top 时，不显示 icon */
			// 	selectedIconPath: 'static/images/home-select.jpeg', /**选中时的图片路径，icon 大小限制为40kb，建议尺寸为 81px * 81px，不支持网络图片。当 position 为 top 时，不显示 icon */
			// },
			{
				pagePath: 'pages/index/index', 
				text: '首页',
				iconPath: 'static/images/home.jpeg',
				selectedIconPath: 'static/images/home-select.jpeg',
			},{
				pagePath: 'pages/leaderboard/index',
				text: '排行榜',
				iconPath: 'static/images/leaderboard.jpeg',
				selectedIconPath: 'static/images/leaderboard-select.jpeg',
			},{
				pagePath: 'pages/my/index',
				text: '我的',
				iconPath: 'static/images/my.jpeg',
				selectedIconPath: 'static/images/my-select.jpeg',
			}
		],
		position: 'bottom',	/**RN、H5不支持 tabBar的位置，仅支持 bottom / top */
		custom: false	/**RN、H5不支持 自定义 tabBar */
	},
	/**网络超时时间 (只支持微信小程序) */
	networkTimeout: {
		request: 60000, /**Taro.request 的超时时间，单位：毫秒 */
		connectSocket: 60000, /**	Taro.connectSocket 的超时时间，单位：毫秒 */
		uploadFile: 60000, /**Taro.uploadFile 的超时时间，单位：毫秒 */
		downloadFile: 60000, /**Taro.downloadFile 的超时时间，单位：毫秒 */
	},
	/**当小程序需要使用 Taro.navigateToMiniProgram 接口跳转到其他小程序时，需要先在配置文件中声明需要跳转的小程序 appId 列表*/
	navigateToMiniProgramAppIdList: [
		{
			appId: '', /**要打开的小程序 appId */
			path: '',/**打开的页面路径，如果为空则打开首页。path 中 ? 后面的部分会成为 query，在小程序的 App.onLaunch、App.onShow 和 Page.onLoad 的回调函数或小游戏的 wx.onShow 回调函数、wx.getLaunchOptionsSync 中可以获取到 query 数据。对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar" */
			extraData: '', /**需要传递给目标小程序的数据，目标小程序可在 App.onLaunch，App.onShow 中获取到这份数据。如果跳转的是小游戏，可以在 wx.onShow、wx.getLaunchOptionsSync 中可以获取到这份数据数据。 */
			envVersion: '', /**要打开的小程序版本。仅在当前小程序为开发版或体验版时此参数有效。如果当前小程序是正式版，则打开的小程序必定是正式版。develop / trial / release*/
			shortLink: '', /**小程序链接，当传递该参数后，可以不传 appId 和 path。链接可以通过【小程序菜单】->【复制链接】获取。 */
			success:  () => { /**接口调用成功的回调函数 */
				
			}, 
			fail:  () => { /**接口调用失败的回调函数 */
				
			}, 
			complete: () => { /**接口调用结束的回调函数（调用成功、失败都会执行） */

			},
		}
	]
	
}