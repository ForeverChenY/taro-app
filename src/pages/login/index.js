import { useState, useEffect }from 'react'
import { View, Image } from '@tarojs/components';
import 	Taro  from '@tarojs/taro';
import { AtButton, AtToast } from 'taro-ui';
import "taro-ui/dist/style/components/button.scss";
import "taro-ui/dist/style/components/loading.scss";
import leaderboard from '../../static/images/home-select.jpeg'
import './index.less'

export default function LoginComponent() {
	const [loading, setLoading] = useState(false);
	const [status, setStatus ]= useState(false);

	useEffect(() => {
		/**登录 */
		const login = () => {
			setLoading(true);
			Taro.login({
				success: (res) => {
					setLoading(false);
					/**或许用户信息 */
					getUserInfo();
					if (res.code) {
						console.log('登录授权成功！' + res.code);
					} else {
						console.error('登录失败！' + res.errMsg);
						setLoading(false);
					}
				},
				complete: (res) => {
					console.log('接口调用结束' + res.errMsg);
					setLoading(false);
				},
				fail: (res) => {
					console.error('接口调用失败' + res.errMsg);
					setLoading(false);
				},
				timeout: (res) => {
					console.error('接口调用超时，请重试' + res.errMsg);
					AtToast('接口调用超时，请重试！');
					setLoading(false);
				},
			});
		}
		login();
	}, [status])
	/**获取用户信息 */
	const getUserInfo = () => {
		Taro.getUserProfile({
			desc: '用于完善用户信息',
			success: (res) => {
				setStatus(true)
				Taro.setStorage({
					key:"userInfo",
					data: JSON.stringify(res.userInfo)
				});
				Taro.navigateBack({
					delta: 1
				})
			}
		});
	}

	return (
		<View className='login'>
			<View className='main'>
				<Image className='logo' src={leaderboard} />
				<View className='title'>运动榜</View>
				<View className='tip'></View>
				<AtButton type='primary' open-type='getUserProfile' size='small' circle loading={loading} onClick={getUserInfo}>微信一键登录</AtButton>
			</View>
		</View>
	)
}
