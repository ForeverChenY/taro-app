import { useState, useEffect } from 'react'
import { View , Map, Image} from '@tarojs/components'
import 	Taro  from '@tarojs/taro';
import "taro-ui/dist/style/components/loading.scss";
import "taro-ui/dist/style/components/button.scss" // 按需引入
import './index.less';

export default function Home() {

	// const mapCtx = Taro.createMapContext('myMap');

	const getStep = () => {
		Taro.getWeRunData({
			success: (res) => {
				console.log(res);
				Taro.cloud.callFunction({
					name: 'myFunction',
					data: {
					  weRunData: Taro.cloud.CloudID(res.cloudID), // 这个 CloudID 值到云函数端会被替换
					  obj: {
						shareInfo: Taro.cloud.CloudID('yyy'), // 非顶层字段的 CloudID 不会被替换，会原样字符串展示
					  }
					}
				  })
			},
			fail: (res) => {
				console.error('接口调用失败', res);
			},
			complete: () => {
				console.log('接口调用结束');
			},
		})
	}

	useEffect( () => {
		const init = async () => {
			await Taro.cloud.init({
				env: 'dev-2g2bbxtzc9c4abf9'
			})
			getStep()
		}
		init();
	})


	return (
		<View className='home'>
			<View className='header'>
				<View className='title'>今日已走</View>
				<View className='info'>
					<View className='number'>13208</View>
					<View className='unit'>步</View>
				</View>
			</View>
			{/* <Image src={header}></Image> */}
			<Map id='myMap' subkey='TLTBZ-LOZKX-MMW4Z-73H4M-NQVRS-4SBWT' longitude='118.744646' latitude='31.998032' scale='14'  layer-style='1'></Map>
		</View>
	)
}
