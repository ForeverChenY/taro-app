import { useState, useEffect } from 'react'
import { View } from '@tarojs/components'
import { AtButton } from 'taro-ui';
import 	Taro  from '@tarojs/taro';
import "taro-ui/dist/style/components/loading.scss";
import "taro-ui/dist/style/components/button.scss" // 按需引入
import './index.less'

export default function Leaderboard() {
	const [list, setList] = useState(false);

	useEffect(() => {
		Taro.shareToWeRun({
			success: (res) => {
				setList(res.encryptedData)
			},
			fail: (res) => {
				console.error('接口调用失败', res);
			},
			complete: () => {
				console.error('接口调用结束');
			},
		})
	})

	return (
		<View className='home'>
		</View>
	)
}
