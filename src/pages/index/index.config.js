export default {
	navigationBarBackgroundColor: '#000000', /**导航栏背景颜色，如 #000000 */
	navigationBarTextStyle: 'white', /**导航栏标题颜色，仅支持 black / white */
	navigationBarTitleText: '首页', /**导航栏标题文字内容 */
	navigationStyle: 'default', /**H5不支持 导航栏样式，仅支持以下值：default 默认样式；custom 自定义导航栏，只保留右上角胶囊按钮 */
	backgroundColor: '#000000', /**H5不支持 窗口的背景色 */
	backgroundTextStyle: 'light', /**H5不支持 下拉 loading 的样式，仅支持 dark / light */
	backgroundColorTop: '#ffffff', /**H5、RN不支持 顶部窗口的背景色，仅 iOS 支持 */
	backgroundColorBottom: '#ffffff', /**H5、RN不支持 底部窗口的背景色，仅 iOS 支持 */
	enablePullDownRefresh: false, /**H5、RN不支持 是否开启当前页面的下拉刷新 */
	onReachBottomDistance: 50, /**H5、RN不支持 页面上拉触底事件触发时距页面底部距离，单位为 px */
	pageOrientation: 'portrait', /**H5、RN不支持 屏幕旋转设置，支持 auto / portrait / landscape 详见 响应显示区域变化 */
	disableScroll: false, /**H5不支持 设置为 true 则页面整体不能上下滚动。只在页面配置中有效，无法在 app.json 中设置 */
	disableSwipeBack: false, /**H5、RN不支持 禁止页面右滑手势返回 */
	usingComponents: '否', /**H5、RN不支持 页面自定义组件配置 */
}
