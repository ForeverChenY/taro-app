/**多平台路区分 */
export const utilsPages = () => {
    if(process.env.TARO_ENV === 'weapp') {
        return require('../app.config.weapp').default;
    } else if(process.env.TARO_ENV === 'h5') {
        return require('../app.config.h5').default;
    } else if(process.env.TARO_ENV === 'rn') {
        return require('../app.config.rn').default;
    }
}
