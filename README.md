# ForeverChen

## 微信小程序

### yarn
<br/>

>yarn dev:weapp 
>
>yarn build:weapp

### npm script
<br/>

>npm run dev:weapp
>
>npm run build:weapp

### 仅限全局安装
<br/>

>taro build --type weapp --watch
>
>taro build --type weapp

### npx 用户也可以使用
<br/>

>npx taro build --type weapp --watch
>
>npx taro build --type weapp

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type weapp --watch # Windows
>
>NODE_ENV=production taro build --type weapp --watch # Mac


##  百度小程序

### yarn
<br/>

>yarn dev:swan
>
>yarn build:swan

### npm script
<br/>

>npm run dev:swan
>
>npm run build:swan

### 仅限全局安装
<br/>

>taro build --type swan --watch
>
>taro build --type swan

### npx 用户也可以使用
<br/>

>npx taro build --type swan --watch
>
>npx taro build --type swan

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type swan --watch # Windows
>
>NODE_ENV=production taro build --type swan --watch # Mac


## 支付宝小程序

### yarn
<br/>

>yarn dev:alipay
>
>yarn build:alipay

### npm script
<br/>

>npm run dev:alipay
>
>npm run build:alipay

### 仅限全局安装
<br/>

>taro build --type alipay --watch
>
>taro build --type alipay

### npx 用户也可以使用
<br/>

>npx taro build --type alipay --watch
>
>npx taro build --type alipay

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type alipay --watch # Windows
>
>NODE_ENV=production taro build --type alipay --watch # Mac


## 字节跳动小程序

### yarn
<br/>

>yarn dev:tt
>
>yarn build:tt

### npm script
<br/>

>npm run dev:tt
>
>npm run build:tt

### 仅限全局安装
<br/>

>taro build --type tt --watch
>
>taro build --type tt


### npx 用户也可以使用
<br/>

>npx taro build --type tt --watch
>
>npx taro build --type tt

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type tt --watch # Windows
>
>NODE_ENV=production taro build --type tt --watch # Mac

## QQ 小程序

### yarn
<br/>

>yarn dev:qq
>
>yarn build:qq

### npm script
<br/>

>npm run dev:qq
>
>npm run build:qq

### 仅限全局安装
<br/>

>taro build --type qq --watch
>
>taro build --type qq

### npx 用户也可以使用
<br/>

>npx taro build --type qq --watch
>
>npx taro build --type qq

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type qq --watch # Windows
>
>NODE_ENV=production taro build --type qq --watch # Mac

## 京东小程序

### yarn
<br/>

>yarn dev:jd
>
>yarn build:jd

### npm script
<br/>

>npm run dev:jd
>
>npm run build:jd

### 仅限全局安装
<br/>

>taro build --type jd --watch
>
>taro build --type jd

### npx 用户也可以使用
<br/>

>npx taro build --type jd --watch
>
>npx taro build --type jd

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type jd --watch # Windows
>
>NODE_ENV=production taro build --type jd --watch # Mac

## 企业微信小程序

### yarn
<br/>

>yarn dev:qywx
>
>yarn build:qywx

### npm script
<br/>

>npm run dev:qywx
>
>npm run build:qywx

### 仅限全局安装
<br/>

>taro build --type qywx --watch
>
>taro build --type qywx

### npx 用户也可以使用
<br/>

>npx taro build --type qywx --watch
>
>npx taro build --type qywx

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type qywx --watch # Windows
>
>NODE_ENV=production taro build --type qywx --watch # Mac

## 钉钉小程序

### yarn 
<br/>

>yarn dev:dd
>
>yarn build:dd

### npm script
<br/>
>npm run dev:dd
>
>npm run build:dd

### 仅限全局安装
<br/>

>taro build --type dd --watch
>
>taro build --type dd

### npx 用户也可以使用
<br/>

>npx taro build --type dd --watch
>
>npx taro build --type dd

### watch 同时开启压缩
<br/>

>set NODE_ENV=production && taro build --type dd --watch # Windows
>
>NODE_ENV=production taro build --type dd --watch # Mac


## 项目目录结构

    ├── dist                        编译结果目录
    |
    ├── config                      项目编译配置目录
    |   ├── index.js                默认配置
    |   ├── dev.js                  开发环境配置
    |   └── prod.js                 生产环境配置
    |
    ├── src                         源码目录
    |   ├── pages                   页面文件目录
    |   |   └── index               index 页面目录
    |   |       ├── index.js        index 页面逻辑
    |   |       ├── index.css       index 页面样式
    |   |       └── index.config.js index 页面配置
    |   |
    |   ├── app.js                  项目入口文件
    |   ├── app.css                 项目总通用样式
    |   └── app.config.js           项目入口配置
    |
    ├── project.config.json         微信小程序项目配置 project.config.json
    ├── project.tt.json             字节跳动小程序项目配置 project.config.json
    ├── project.swan.json           百度小程序项目配置 project.swan.json
    ├── project.qq.json             QQ 小程序项目配置 project.config.json
    |
    ├── babel.config.js             Babel 配置
    ├── tsconfig.json               TypeScript 配置
    ├── .eslintrc                   ESLint 配置
    |
    └── package.json
    |
    └── theme.json                  对应主题下的颜色