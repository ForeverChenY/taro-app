import { Component } from 'react'
import './app.less'

class App extends Component {

    /**
     * @name 在微信/百度/字节跳动/支付宝小程序中这一生命周期方法对应app的onLaunch
     * @param {String} path 启动小程序的路径
     * @param {Number} scene 启动小程序的场景值  微信小程序和百度小程序中存在区别
     * @param {Object} query 	启动小程序的 query 参数
     * @param {String} shareTicket 详见获取更多转发信息
     * @param {Object} referrerInfo  来源信息。从另一个小程序、公众号或 App 进入小程序时返回。否则返回 {}
     */
    componentWillMount (path, scene, query, shareTicket, referrerInfo) {

    }
	/**
	 * @name 在微信/百度/字节跳动/支付宝小程序中这一生命周期方法对应app的onLaunch，在componentWillMount后执行,监听程序初始化，初始化完成时触发（全局只触发一次）
	 * @param {String} path 启动小程序的路径
     * @param {Number} scene 启动小程序的场景值  微信小程序和百度小程序中存在区别
     * @param {Object} query 	启动小程序的 query 参数
     * @param {String} shareTicket 详见获取更多转发信息
     * @param {Object} referrerInfo  来源信息。从另一个小程序、公众号或 App 进入小程序时返回。否则返回 {}
    */ 
    componentDidMount (path, scene, query, shareTicket, referrerInfo) {

	}

	/**
	 * @name 在微信/百度/字节跳动/支付宝小程序中这一生命周期方法对应onShow，在H5/RN中同步实现 
	 * @param {String} path 启动小程序的路径
     * @param {Number} scene 启动小程序的场景值  微信小程序和百度小程序中存在区别
     * @param {Object} query 	启动小程序的 query 参数
     * @param {String} shareTicket 详见获取更多转发信息
	 * @param {Object} referrerInfo  来源信息。从另一个小程序、公众号或 App 进入小程序时返回。否则返回 {}
	 * @param {String} entryType  展现的来源标识，取值为 user/ schema /sys :
						user：表示通过home前后
						切换或解锁屏幕等方式调起；
						schema：表示通过协议调起;
						sys：其它
	 * @param {String} appURL  展现时的调起协议，仅当entryType值为 schema 时存
	 */
    componentDidShow (path, scene, query, shareTicket, referrerInfo, entryType, appURL) {

	}

    componentDidHide () {}

    componentDidCatchError () {}

    // this.props.children 是将要会渲染的页面
    render () {
      	return this.props.children
    }
}

export default App
